var faker = require('faker');
var expect = require('chai').expect;
var assert = require('chai').assert;
var { Given, Then, When } = require('cucumber');
var { setDefaultTimeout } = require('cucumber');
setDefaultTimeout(60 * 1000);

Given('User navigates to url: {string}', async function (url) {
    await browser.get(url);
});

When('User enters the following username: {string} and password {string}', async function (username, password) {
    element(by.id('txtUsername')).sendKeys(username);
    element(by.id('txtPassword')).sendKeys(password);
    await browser.sleep(500);
    element(by.id('btnLogin')).click();
});

Then('User should see welcome message: {string}', async function (expectedMessage) {
    await browser.sleep(3000);
    let actualMessage = element(by.id('welcome')).getText();
    await actualMessage.then(function (actual) {
        expect(actual).to.equal(expectedMessage, 'Expected value is: ' + expectedMessage + '! Actual value is: ' + actual + "!");
    });
});

When('User navigates to page: {string}', async function (page) {
    let expectedPage;

    switch (page) {
        case "usersPage":
            element(by.id("menu_admin_viewAdminModule")).click();
            await browser.sleep(1000);
            expectedPage = "viewSystemUsers";
            validatePage(expectedPage);
            break;
        case "leavePage":
            element(by.id("menu_leave_viewLeaveModule")).click();
            await browser.sleep(1000);
            element(by.id('menu_leave_assignLeave')).click();
            await browser.sleep(1000);
            expectedPage = "assignLeave";
            validatePage(expectedPage);
            break;
        default:
            assert.fail("Page : " + page + " doesn't exist!")
            break;
    }
});

async function validatePage(expectedPage) {
    await browser.getCurrentUrl().then(url => {
        var page = url.substring(url.lastIndexOf("/") + 1, url.length);
        expect(page).to.equal(expectedPage, 'Expected value is: ' + expectedPage + '! Actual value is: ' + page + "!");
    });
}

Then('User creates new user with username: {string} and password: {string}', async function (username, password) {
    element(by.id('btnAdd')).click();
    await browser.sleep(1000);

    element(by.id('systemUser_employeeName_empName')).sendKeys("Hannah Flores");
    if (username == 'random') {
        element(by.id('systemUser_userName')).sendKeys(faker.name.firstName());
    } else {
        element(by.id('systemUser_userName')).sendKeys(username);
    }
    element(by.id('systemUser_password')).sendKeys(password);
    element(by.id('systemUser_confirmPassword')).sendKeys(password);
    element(by.id('btnSave')).click();
});

Then('User creates new leave with the following data', async function (table) {
    const input = table.hashes();
    element(by.id('assignleave_txtComment')).sendKeys(input[0].comment);
    element(by.id('assignleave_txtToDate')).click().sendKeys(input[0].to);
    element(by.id('assignleave_txtLeaveType')).click().sendKeys(input[0].leaveType);
    await browser.actions().sendKeys(protractor.Key.ENTER).perform();
    element(by.id('assignleave_txtFromDate')).click().sendKeys(input[0].from);
    element(by.id('assignleave_txtEmployee_empName')).sendKeys(input[0].name);
    element(by.id('assignBtn')).click();
});