Feature: Login, creating user and adding leave

    Scenario Outline: 1. Log in
        Given User navigates to url: "https://opensource-demo.orangehrmlive.com/index.php/auth/login"
        When User enters the following username: <username> and password <password>
        Then User should see welcome message: <message>

        @smoke
        Examples:
            | username | password   | message         |
            | "Admin"  | "admin123" | "Welcome Admin" |


    Scenario Outline: 2. Login and create user
        Given User navigates to url: "https://opensource-demo.orangehrmlive.com/index.php/auth/login"
        When User enters the following username: <username> and password <password>
        Then User should see welcome message: <message>
        When User navigates to page: "usersPage"
        Then User creates new user with username: "random" and password: "Kragujevac"

        @smoke
        Examples:
            | username | password   | message         |
            | "Admin"  | "admin123" | "Welcome Admin" |


    Scenario Outline: 3. Login and add leave
        Given User navigates to url: "https://opensource-demo.orangehrmlive.com/index.php/auth/login"
        When User enters the following username: <username> and password <password>
        Then User should see welcome message: <message>
        When User navigates to page: "leavePage"
        Then User creates new leave with the following data
            | name           | leaveType    | from       | to         | comment         |
            | Jasmine Morgan | Maternity US | 2020-05-17 | 2020-05-20 | Maternity leave |

        @smoke
        Examples:
            | username | password   | message         |
            | "Admin"  | "admin123" | "Welcome Admin" |


